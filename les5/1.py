import time
import sys


class TestManager:
    start_time = time.time()

    def __enter__(self):
        #self.start_time = time.time()
        print("hi")

    def __exit__(self, exc_type, exc_val, exc_tb):
        execute_time = time.time() - self.start_time
        print('Код выполнился. Время выполнения - %f. Инфа об ошибке: тип - %s' % (execute_time, exc_type))

with TestManager():
    time.sleep(1)
    print('Hello')
    print(__name__)
