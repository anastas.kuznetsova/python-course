import itertools


def union_arrays(arr1, arr2, arr3):
    return list(itertools.chain(arr1, arr2, arr3))


def len_more5(my_arr):
    return list(itertools.filterfalse(lambda x: len(x) < 5, my_arr))


def password_var1(my_str):
    return list(itertools.permutations(my_str, 4))


def password_var2(my_str):
    str1 = my_str[0:3]
    str2 = itertools.islice(my_str, 3, None)
    my_list = []
    for i in str2:
        my_list.append(tuple(itertools.chain(str1, i)))
    return my_list


print(union_arrays([1, 2, 3], [4, 5], [6, 7]))
print(len_more5(['hello', 'i', 'write', 'cool', 'code']))
print(password_var1('password'))
print(password_var2('password'))
