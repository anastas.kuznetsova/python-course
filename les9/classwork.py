import math
import zipfile
import os


def pythagoras(catheter1, catheter2):
    if type(catheter1) in (int, float) and type(catheter2) in (int, float) \
            and catheter1 > 0 and catheter2 > 0:
        hypotenuse = round(math.sqrt(catheter1**2 + catheter2**2), 2)
        return hypotenuse


class FileNotExistsException(Exception):
    def __init__(self, file_name):
        self.txt = 'File %s is\'nt exists' % file_name


def my_zip(*args):
    z = zipfile.ZipFile('/home/nastya/test/myzip', 'w')
    for i in args:
        try:
            if not os.path.exists(i):
                raise FileNotExistsException(my_dir)
            z.write(i, os.path.basename(i))
        except FileNotExistsException:
            continue
    z.close()

        #with zipfile.ZipFile() as myzip:
        #    myzip.write()


my_zip('/home/nastya/test/test_zip1.txt', '/home/nastya/test/test_zip2.txt')
#print(pythagoras(2, 5))
