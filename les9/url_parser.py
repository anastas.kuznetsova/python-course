import urllib.parse
import multiprocessing
from concurrent.futures import ThreadPoolExecutor, as_completed
from html.parser import HTMLParser
from urllib.request import urlopen


class TitleParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.match = False
        self.title = ''

    def handle_starttag(self, tag, attributes):
        self.match = True if tag == 'title' else False

    def handle_data(self, data):
        if self.match:
            self.title = data
            self.match = False


#def get_title(url):
#    response = urllib.request.urlopen(url)
#    soup = BeautifulSoup(response)
#    return soup.title.string


class ParseUrl:
    def __init__(self, *args):
        self.list_url = []
        for i in args:
            if self.url_validate(i):
                self.list_url.append(i)

    def get_list_of_titles(self):

        def get_title(url):
            try:
                response = urlopen(url)
                parser = TitleParser()
                encoding = response.headers.get_content_charset()
                if encoding:
                    parser.feed(response.read().decode(encoding))
                    return parser.title
            except Exception as e:
                print(e)
                return

        list_of_titles = []
        cpu_count = multiprocessing.cpu_count()
        with ThreadPoolExecutor(max_workers=cpu_count) as pool:
            results = [pool.submit(get_title, i) for i in self.list_url]

            for future in as_completed(results):
                list_of_titles.append(future.result())
        return list_of_titles

    def url_validate(self, my_url):
        try:
            result = urllib.parse.urlparse(my_url)
            return result.scheme and result.netloc
        except:
            return False


my_parse_url = ParseUrl('https://www.google.ru', 'https://stackoverflow.com', 'https://habr.com', 'https://docs.python.org',
              'http://qaru.site/questions', 'https://ru.wikipedia.org', 'https://yandex.ru/',
              'https://www.the-village.ru/',
              'https://learn.lingvist.com', 'https://www.youtube.com/', 'https://www.livejournal.com/',
              'https://translate.google.com', 'https://gitlab.com', 'https://www.blogger.com',
              'https://news.yandex.ru/', 'https://yandex.ru/maps', 'https://market.yandex.ru',
              'https://translate.yandex.ru/', 'https://music.yandex.ru/', 'https://disk.yandex.ru/')
print(my_parse_url.get_list_of_titles())

