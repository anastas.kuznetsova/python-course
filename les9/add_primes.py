import threading
import multiprocessing as mp
import queue
from datetime import datetime


my_args = [(3, 1000), (1001, 2000), (2001, 3000)]


def get_time_decorator(my_func):
    def wrapper(*args):
        time_start = datetime.now()
        my_func(*args)
        time_execute = (datetime.now() - time_start).total_seconds()
        print('Time: %s' % time_execute)
    return wrapper


def add_primes(start, end):
    primes = []
    position_start = 0
    for num in range(start, end + 1):
        for i in range(2, num + 1):
            if (i > 10) and ((i % 2 == 0) or (i % 10 == 5)):
                continue
            for j in primes:
                if i % j == 0:
                    break
            else:
                primes.append(i)

    for i, num in enumerate(primes):
        if num == start or (num > start and position_start == 0):
            position_start = i
            break

    return primes[position_start:]

#Time: 18.285987
@get_time_decorator
def simple_start():
    for i in my_args:
        print(add_primes(i[0], i[1]))

#Time: 26.472165
@get_time_decorator
def thread_start():
    que = queue.Queue()
    threads = []

    for i in my_args:
        #thr = threading.Thread(target=add_primes, args=i)
        thr = threading.Thread(target=lambda q, arg: q.put(add_primes(arg[0], arg[1])), args=(que, i))
        thr.start()
        threads.append(thr)

    for thr in threads:
        thr.join()

    while not que.empty():
        result = que.get()
        print(result)

#Time: 12.514665
@get_time_decorator
def mp_start():
    procs = []

    for i in my_args:
        proc = mp.Process(target=add_primes, args=i)
        procs.append(proc)
        proc.start()

    for proc in procs:
        proc.join()


#simple_start()
#thread_start()
mp_start()







