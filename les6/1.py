import pathlib
import os

def my_len(my_list):
    if type(my_list) not in [list, tuple, dict]:
        print('It\'s not list!!')
        return
    len_list = 0
    for i in my_list:
        len_list += 1

    return len_list


def my_reversed(my_list):
    if type(my_list) not in [list, tuple, dict]:
        print('It\'s not list!!')
        return
    len_list = len(my_list)
    i = len_list - 1
    reverse_list = []
    while i >= 0:
        reverse_list.append(my_list[i])
        i -= 1
    return reverse_list


def to_title(my_str):
    words = my_str.split(' ')
    new_str = ''
    for i in words:
        new_str += ' ' + i[0].upper() + i[1:]
    return new_str.lstrip()


def count_symbol(my_str, my_symbol):
    cnt = 0
    for i in my_str:
        if i == my_symbol:
            cnt += 1
    return cnt


class FileExistsException(Exception):
    def __init__(self, file_name):
        self.txt = 'File %s already exists' % file_name


def copyfile(from_file, to_file):
    pth_from = pathlib.Path(from_file)
    pth_to = pathlib.Path(to_file)

    if not pth_from.is_file():
        print('File %s doesn\'t exist' % from_file)
        return

    try:
        if pth_to.is_file():
            raise FileExistsException(to_file)
        pth_to.write_bytes(pth_from.read_bytes())
    except IndentationError:
        print('Incomplete character string!')
        return
    except IsADirectoryError:
        print('It\'s directory!')
        return
    except FileExistsException as ex:
        print(ex.txt)
        return
    except Exception as e:
        print("Unexpected error: " + str(e))
        return


def copydir(from_dir, to_dir):
    pth_from = pathlib.Path(from_dir)

    if not pth_from.is_dir():
        print('Directory %s doesn\'t exist' % from_dir)
        return

    if not os.path.isdir(to_dir):
        print('Directory %s doesn\'t exist' % to_dir)
        return

    if pth_from.absolute() == os.path.dirname(to_dir):
        print('Enter different directories!')
        return

    for f in pth_from.iterdir():
        if f.is_file():
            new_file = os.path.abspath(to_dir) + '/%s' % f.name
            copyfile(f.absolute(), new_file)

        if f.is_dir():
            new_dir = os.path.abspath(to_dir) + '/%s' % f.name
            if not os.path.isdir(new_dir):
                os.makedirs(new_dir)
                copydir(f.absolute(), new_dir)
            else:
                continue


print(my_len((1, 2, 3, 4, -5)))
print(my_reversed([2, 5, 7, 9]))
print(to_title('nas tya pyt hon'))
print(count_symbol(' jkljlkj ', ' '))
copyfile('/home/nastya/test_path1/cat.jpg', '/home/nastya/test_path2/cat')
copydir('/home/nastya/test_path1/', '/home/nastya/test_path2/')

