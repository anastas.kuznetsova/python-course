class ValidVelueException(Exception):
    def __init__(self, text_obj):
        self.txt = '%s must be positive integer' % text_obj


class Money():
    def __init__(self, ruble=0, kopeck=0):
        try:
            if not isinstance(ruble, int) or ruble < 0:
                raise ValidVelueException('Ruble')

            if not isinstance(kopeck, int) or kopeck < 0:
                raise ValidVelueException('Kopeck')

            self.ruble = ruble
            self.kopeck = kopeck
        except ValidVelueException as ex:
            print(ex.txt)
            return

    def __str__(self):
        return '%d,%d' % (self.ruble, self.kopeck)

    def __float__(self):
        return float('%d.%d' % (self.ruble, self.kopeck))

    def __add__(self, other):
        return round(float(self) + float(other), 5)

    def __sub__(self, other):
        return round(float(self) - float(other), 5)

    def __truediv__(self, other):
        return round(float(self) / float(other), 5)

    def __eq__(self, other):
        return float(self) == float(other)

    def __ne__(self, other):
        return not (float(self) == float(other))

    def __lt__(self, other):
        return float(self) < float(other)

    def __le__(self, other):
        return float(self) <= float(other)

    def __gt__(self, other):
        return float(self) > float(other)

    def __ge__(self, other):
        return float(self) >= float(other)

    def setExchangeRatesDollar(self, value):
        try:
            if type(value) not in (int, float) or value < 0:
                raise ValidVelueException('Exchange rates')
            self.exchange_rates_dollar = value
        except ValidVelueException as ex:
            print(ex.txt)
            return

    def getExchangeRatesDollar(self):
        try:
            return self.exchange_rates_dollar
        except Exception as ex:
            print(str(ex))
            return

    def convertIntoDollar(self):
        if self.getExchangeRatesDollar():
            return round(float(self) * self.getExchangeRatesDollar(), 5)


my_money = Money()
my_money.ruble = 1000
my_money.kopeck = 20
my_money2 = Money()
my_money2.ruble = 1052588
my_money2.kopeck = 1564488775
print(my_money + my_money2)
print(my_money2 - my_money)
print(my_money - my_money2)
print(my_money2 / my_money)
print(my_money2 == my_money)
print(my_money2 != my_money)
print(my_money < my_money2)
my_money.setExchangeRatesDollar(65)
print(my_money.convertIntoDollar())