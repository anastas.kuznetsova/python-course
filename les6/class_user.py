class ValidNumberException(Exception):
    def __init__(self, text_obj, text):
        self.txt = '%s must be %s' % (text_obj, text)


class ValidDrivingCategory(Exception):
    def __init__(self, category):
        self.txt = 'This driving category %s isn\'t valid. It must be A or B or C' % category


class User:
    def setName(self, name):
        if not name:
            print('Name must be filled out')
            return
        self.name = str(name)

    def getName(self):
        try:
            return self.name
        except Exception as ex:
            print(str(ex))
            return

    def setAge(self, age):
        try:
            if type(age) not in (int, float) or age < 0:
                raise ValidNumberException('Age', 'positive number')
            self.age = age
        except ValidNumberException as ex:
            print(ex.txt)
            return

    def getAge(self):
        try:
            return self.age
        except Exception as ex:
            print(str(ex))
            return 0


class Worker(User):
    def setSalary(self, salary):
        try:
            if type(salary) not in (int, float) or salary < 0:
                raise ValidNumberException('Salary', 'positive number')
            self.salary = salary
        except ValidNumberException as ex:
            print(ex.txt)
            return

    def getSalary(self):
        try:
            return self.salary
        except Exception as ex:
            print(str(ex))
            return 0


class Student(User):
    def setStipend(self, stipend):
        try:
            if type(stipend) not in (int, float) or stipend < 0:
                raise ValidNumberException('Stipend', 'positive number')
            self.stipend = stipend
        except ValidNumberException as ex:
            print(ex.txt)
            return

    def getStipend(self):
        try:
            return self.stipend
        except Exception as ex:
            print(str(ex))
            return 0

    def setCouse(self, course):
        try:
            if not isinstance(course, int) or course > 6:
                raise ValidNumberException('Course', 'positive integer number =< 6')
            self.course = course
        except ValidNumberException as ex:
            print(ex.txt)
            return

    def getCourse(self):
        try:
            return self.course
        except Exception as ex:
            print(str(ex))
            return


class Driver(Worker):
    def setDrivingExperience(self, years):
        try:
            if type(years) not in (int, float) or years < 0:
                raise ValidNumberException('DrivingExperience', 'positive number')
            self.driving_experience = years
        except ValidNumberException as ex:
            print(ex.txt)
            return

    def getDrivingExperience(self):
        try:
            return self.driving_experience
        except Exception as ex:
            print(str(ex))
            return 0

    def setDrivingCategory(self, category):
        category = str(category).replace(' ', '').upper()
        try:
            if category not in ('A', 'B', 'C'):
                raise ValidDrivingCategory(category)
            self.driving_category = category
        except ValidDrivingCategory as ex:
            print(ex.txt)
            return

    def getDrivingCategory(self):
        try:
            return self.driving_category
        except Exception as ex:
            print(str(ex))
            return


my_user = Worker()
my_user.setName('Иван')
my_user.setAge(25)
my_user.setSalary(1000)

my_user2 = Worker()
my_user2.setName('Вася')
my_user2.setAge(26)
my_user2.setSalary(2000)
sum_salary = my_user.getSalary() + my_user2.getSalary()
print(sum_salary)

my_user3 = Driver()
my_user3.setName('Кристофор')
my_user3.setDrivingCategory('A')

my_user4 = Student()
my_user4.setName('Константин')
my_user4.setCouse(6)
my_user4.getCourse()
my_user4.getStipend()