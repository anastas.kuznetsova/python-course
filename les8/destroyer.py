import os
import shutil
import datetime


class FileNotExistsException(Exception):
    def __init__(self, file_name):
        self.txt = 'File %s is\'nt exists' % file_name


def follow_dir(my_dir):
    try:
        if not os.path.isdir(my_dir):
            raise FileNotExistsException(my_dir)
    except FileNotExistsException:
        return

    while True:
        for i in os.walk(my_dir):
            if i[2]:
                for file in i[2]:
                    time_create = datetime.datetime.fromtimestamp(os.path.getctime(i[0]+'/'+file))
                    if time_create + datetime.timedelta(minutes=1) < datetime.datetime.now():
                        os.remove(i[0]+'/'+file)
                continue

            time_create = datetime.datetime.fromtimestamp(os.path.getctime(i[0]))
            if time_create + datetime.timedelta(minutes=2) < datetime.datetime.now():
                shutil.rmtree(i[0])


follow_dir('/home/kuznecova_as/tests/destdir')
