import requests
import datetime
import json
import os
import traceback



def write_log(msg):
    log_file = '/home/nastya/test/weather.log'
    now_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    if os.path.exists(log_file):
        with open(log_file, 'a') as file:
            file.write("\n\n%s: %s" % (now_time, msg))


class ForecastWeather:
    def __init__(self):
        try:
            self.region_name, self.lat, self.lon = self.get_my_coordinate()
        except TypeError:
            self._weather_info = ''
            return

        yandex_secret_key = '67697b64-2fa0-4b11-bc43-4cf546655596'
        api_yandex_url = 'https://api.weather.yandex.ru/v1/forecast?lat=%s&lon=%s&extra=true' % (self.lat, self.lon)
        headers = {'X-Yandex-API-Key': yandex_secret_key}

        try:
            weather_response = requests.get(api_yandex_url, headers=headers)
        except requests.exceptions.RequestException as err:
            err_msg = "Something wrong: %s \n" % str(err) + traceback.format_exc()
            write_log(err_msg)
            self._weather_info = ''
            return
        except requests.exceptions.HTTPError as err:
            err_msg = "Http Error: %s \n" % str(err) + traceback.format_exc()
            write_log(err_msg)
            self._weather_info = ''
            return
        except requests.exceptions.ConnectionError as err:
            err_msg = "Error Connecting: %s \n" % str(err) + traceback.format_exc()
            write_log(err_msg)
            self._weather_info = ''
            return
        except requests.exceptions.Timeout as err:
            err_msg = "Timeout Error: %s \n" % str(err) + traceback.format_exc()
            write_log(err_msg)
            self._weather_info = ''
            return
        self._weather_info = weather_response.json()

    @staticmethod
    def get_my_ip():
        try:
            ip_info = requests.get('https://ident.me')
            return ip_info.text
        except requests.exceptions.RequestException as err:
            err_msg = "Something wrong: %s \n" % str(err) + traceback.format_exc()
            write_log(err_msg)
        except requests.exceptions.HTTPError as err:
            err_msg = "Http Error: %s \n" % str(err) + traceback.format_exc()
            write_log(err_msg)
        except requests.exceptions.ConnectionError as err:
            err_msg = "Error Connecting: %s \n" % str(err) + traceback.format_exc()
            write_log(err_msg)
        except requests.exceptions.Timeout as err:
            err_msg = "Timeout Error: %s \n" % str(err) + traceback.format_exc()
            write_log(err_msg)

    def get_my_coordinate(self):
        if not self.get_my_ip():
            return

        try:
            place_response = requests.get('http://ip-api.com/json/%s' % self.get_my_ip())
        except requests.exceptions.RequestException as err:
            err_msg = "Something wrong: %s \n" % str(err) + traceback.format_exc()
            write_log(err_msg)
        except requests.exceptions.HTTPError as err:
            err_msg = "Http Error: %s \n" % str(err) + traceback.format_exc()
            write_log(err_msg)
        except requests.exceptions.ConnectionError as err:
            err_msg = "Error Connecting: %s \n" % str(err) + traceback.format_exc()
            write_log(err_msg)
        except requests.exceptions.Timeout as err:
            err_msg = "Timeout Error: %s \n" % str(err) + traceback.format_exc()
            write_log(err_msg)

        if not place_response:
            return

        place_info = place_response.json()
        city_name = place_info.get('city')
        if not city_name:
            city_name = 'No city name'
        # широта
        lat = place_info.get('lat')
        # долгота
        lon = place_info.get('lon')
        return city_name, lat, lon

    @property
    def time(self):
        return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    @property
    def temp(self):
        if not self._weather_info:
            return ''

        return self._weather_info.get('fact').get('temp')

    @property
    def temp_feelslike(self):
        if not self._weather_info:
            return ''

        return self._weather_info.get('fact').get('feels_like')

    @property
    def precipitation(self):
        if not self._weather_info:
            return ''
        prectype_txt = {
            0: 'without precipitation',
            1: 'rain',
            2: 'rain with snow',
            3: 'snow',
        }
        precipitation = self._weather_info.get('fact').get('prec_type')
        return prectype_txt.get(precipitation)

    def forecast_dict(self):
        forecast = {}
        forecast.update({'time': self.time, 'temp': self.temp,
                         'temp_feelslike': self.temp_feelslike, 'precipitation': self.precipitation,
                         'region_name': self.region_name,})
        return forecast

    def forecast_json(self):
        return json.dumps(self.forecast_dict())

    def __str__(self):
        if not self._weather_info:
            return '%s  ERROR HAS OCCURED! Open weather.log' % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        content = ''
        forecast = self.forecast_dict()
        for i in forecast:
            content += '%s:%s\n' % (i, forecast.get(i))
        return content

