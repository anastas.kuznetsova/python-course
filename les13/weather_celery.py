import weather
import os
from celery import Celery
from datetime import timedelta


app = Celery('weather_celery')
app.conf.update(
    BROKER_URL='amqp://localhost',
    CELERYBEAT_SCHEDULE={
        'write_forecast_tofile':{
            'task': 'weather_celery.write_forecast_tofile',
            'schedule': timedelta(minutes=5)
        }
    }
)


@app.task
def write_forecast_tofile():
    file_name = '/home/nastya/test/weather.txt'
    forecast_now = weather.ForecastWeather()
    if os.path.exists(file_name):
        with open(file_name, 'a') as file:
            file.write("\n\n%s" % str(forecast_now))
