arr = [0, 3, 24, 2, 3, 7]

len_arr = len(arr)
i = 0

while i < len_arr:
    min_num = arr[i]
    for j, num in enumerate(arr[i+1:]):
        if num < min_num:
            min_num = num
            pos = j + i + 1
            arr[i], arr[pos] = arr[pos], arr[i]
    i += 1

print(arr)
