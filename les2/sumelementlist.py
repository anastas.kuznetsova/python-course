def get_sum1(my_list1):
    sum_list = 0
    for i in my_list1:
        sum_list += i
    return sum_list


def get_sum2(my_list2):
    len_list = len(my_list2)
    sum_list = 0
    i = 0
    while i < (len_list):
        sum_list += my_list2[i]
        i += 1
    return sum_list


my_list = [1, 2, 5, 10, -1]
print(get_sum2(my_list))
print(get_sum1(my_list))