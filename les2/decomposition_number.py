def decomposition(my_number):
    if not my_number.isdigit():
        print('Данная строка не является целым числом')
        return

    if not len(my_number) == 5:
        print('Число не 5-значное. Давайте еще раз.')
        return

    for i, num in enumerate(my_number, 1):
        print('{} цифра равна {}'.format(i, num))


my_number = input('Введите любое 5-значное число ')
decomposition(my_number)
